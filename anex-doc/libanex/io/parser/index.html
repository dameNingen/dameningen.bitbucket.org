<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="rustdoc">
    <meta name="description" content="API documentation for the Rust `parser` mod in crate `libanex`.">
    <meta name="keywords" content="rust, rustlang, rust-lang, parser">

    <title>libanex::io::parser - Rust</title>

    <link rel="stylesheet" type="text/css" href="../../../main.css">

    <link rel="shortcut icon" href="https://dameningen.bitbucket.org/pix/anexLogo.png">
    
</head>
<body class="rustdoc">
    <!--[if lte IE 8]>
    <div class="warning">
        This old browser is unsupported and will most likely display funky
        things.
    </div>
    <![endif]-->

    

    <section class="sidebar">
        <a href='../../../libanex/index.html'><img src='https://dameningen.bitbucket.org/pix/anexLogo.png' alt='' width='100'></a>
        <p class='location'><a href='../../index.html'>libanex</a>::<wbr><a href='../index.html'>io</a></p><script>window.sidebarCurrent = {name: 'parser', ty: 'mod', relpath: '../'};</script><script defer src="../sidebar-items.js"></script>
    </section>

    <nav class="sub">
        <form class="search-form js-only">
            <div class="search-container">
                <input class="search-input" name="search"
                       autocomplete="off"
                       placeholder="Click or press 'S' to search, '?' for more options..."
                       type="search">
            </div>
        </form>
    </nav>

    <section id='main' class="content mod">
<h1 class='fqn'><span class='in-band'>Module <a href='../../index.html'>libanex</a>::<wbr><a href='../index.html'>io</a>::<wbr><a class='mod' href=''>parser</a><wbr></span><span class='out-of-band'><span id='render-detail'>
            <a id="collapse-all" href="#">[-]</a>&nbsp;<a id="expand-all" href="#">[+]</a>
        </span><a id='src-1088' href='../../../src/libanex/io/parser.rs.html#1-1469'>[src]</a></span></h1>
<div class='docblock'><p>Purely functional parser combinators library.</p>

<p>A purely functional parser combinators library. Uses the <code>Lexer</code> and
<code>LexerCursor</code> from the <code>lexer</code> module.</p>

<p>The standard way to create parsers is to use the <code>parser</code> macro,
which look like this:</p>
<pre id='rust-example-rendered' class='rust '>
<span class='macro'>parser</span><span class='macro'>!</span>(
    <span class='kw'>fn</span> <span class='ident'>ident</span> : <span class='ident'>Type</span> { <span class='ident'>combinator_parser</span> }
) ;
</pre>

<p>This creates a function called <code>ident</code> taking a <code>&amp; LexerCursor</code> and
returning a <code>parser::Result&lt;Type&gt;</code>. This result type is either</p>

<ul>
<li><code>Ok( result, lexer )</code> where <code>result</code> is the result and <code>lexer</code> is
the new state of the lexer, or</li>
<li><code>Err( failure )</code> where failure is a <code>ParseFailure</code>, <em>i.e.</em>

<ul>
<li><code>Msg( string )</code> with <code>string</code> a description of the failure, or</li>
<li><code>EoF</code> meaning the parser reached end of file before it was done.</li>
</ul></li>
</ul>

<p>It is recommended to use the <code>parse</code> and <code>parse_all</code> functions when
actually parsing something. They take a <code>Lexer</code> and return a
<code>std::result::Result&lt;Type, ParseFailure&gt;</code>.</p>

<p>The following section introduces the syntax for <code>combinator_parser</code>.</p>

<h1 id="syntax" class='section-header'><a
                           href="#syntax">Syntax</a></h1>
<p>The following parser combinators can be nested arbitrarily, except
obviously tokens and identifiers. There is <strong>no</strong> priority between
combinators so any parser combination must be parenthesized to be
combined with something else. In the following, syntax snippets of
the form <code>( &lt;comb&gt; )</code> stand for a combination of parsers between
parens or a token / identifier.</p>

<p><strong>Tokens</strong> are pretty straightforward: <code>[&quot;bla&quot;]</code> parses a token
(<code>bla</code> here) and returns a <code>lexer::Token</code>.</p>

<p><strong>Regex</strong> look like <code>{r&quot;^\d{4}-\d{2}-\d{2}$&quot;}</code>. They use the
<code>regex</code> lib from the rust distribution, refer to
<a href="http://doc.rust-lang.org/regex/regex/index.html" title="Documentation of the regex crate.">its documentation</a> for more details.</p>

<p><strong>Identifiers</strong> refer to previously defined parsers: <code>my_parser</code>.</p>

<p><strong>Repetitions</strong> of zero or more matches <code>( &lt;comb&gt; )*</code> or of one
or more matches <code>( &lt;comb&gt; )+</code>. If successful, it produces a
vector of whatever the type of <code>&lt;comb&gt;</code> is.</p>

<p><strong>Branches</strong> are kind of Caml style:</p>
<pre id='rust-example-rendered' class='rust '>
<span class='op'>|</span> (<span class='op'>&lt;</span><span class='ident'>comb_1</span><span class='op'>&gt;</span>)
<span class='op'>|</span> (<span class='op'>&lt;</span><span class='ident'>comb_2</span><span class='op'>&gt;</span>)
...
<span class='op'>|</span> (<span class='op'>&lt;</span><span class='ident'>comb_n</span><span class='op'>&gt;</span>)
</pre>

<p>Note that all branches must be of the same type for the resulting
parser to make sense.</p>

<p><strong>Sequences</strong> of parsers are comma separated <code>(&lt;comb_1&gt;), (&lt;comb_2&gt;),
...)</code> and, if successful, produce tuples of the expected type.</p>

<p><strong>Ignoring</strong> part of a sequence is done with the <code>&gt;</code> and <code>&lt;</code> symbols:</p>
<pre id='rust-example-rendered' class='rust '>
(<span class='op'>&lt;</span><span class='ident'>ignored</span><span class='op'>&gt;</span>  ) <span class='op'>&gt;</span> (<span class='op'>&lt;</span><span class='ident'>kept</span><span class='op'>&gt;</span>)
                (<span class='op'>&lt;</span><span class='ident'>kept</span><span class='op'>&gt;</span>) <span class='op'>&lt;</span> (<span class='op'>&lt;</span><span class='ident'>ignored</span><span class='op'>&gt;</span>  )
(<span class='op'>&lt;</span><span class='ident'>ignored_1</span><span class='op'>&gt;</span>) <span class='op'>&gt;</span> (<span class='op'>&lt;</span><span class='ident'>kept</span><span class='op'>&gt;</span>) <span class='op'>&lt;</span> (<span class='op'>&lt;</span><span class='ident'>ignored_2</span><span class='op'>&gt;</span>)
</pre>

<p>When successful, the result has the type of <code>&lt;kept&gt;</code>.</p>

<p><strong>Modification by a function</strong> is specified in three parts separated
by <code>=&gt;</code>: <code>(&lt;comb&gt;) =&gt; &lt;binding&gt; =&gt; &lt;block&gt;</code>.</p>

<ul>
<li><code>(&lt;comb&gt;)</code> is a combination of parsers,</li>
<li><code>&lt;binding&gt;</code> binds / destructures what <code>(&lt;comb&gt;)</code> returns, and</li>
<li><code>&lt;block&gt;</code> is a block using the bindings from <code>&lt;binding&gt;</code> to produce
some value.</li>
</ul>

<p><strong>N.B.</strong> The arity of the <em>sequence</em> and <em>branch</em> combinators is
limited to eleven.</p>

<h1 id="the-parseable-trait" class='section-header'><a
                           href="#the-parseable-trait">The <code>Parseable</code> trait</a></h1>
<p>Note that a <code>Parseable</code> trait is defined in <code>io</code> for <code>&amp; str</code>,
<code>String</code>, <code>Vec&lt;String&gt;</code>, <code>std::path::Path</code>, and <code>std::fs::File</code>. It
implements the <code>parsex</code> and <code>parsex_all</code> functions for these types.</p>

<h1 id="examples" class='section-header'><a
                           href="#examples">Examples</a></h1>
<p>Parsing a non-empty repetition of <code>&quot;bla&quot;</code> between matching curly braces.</p>
<pre id='rust-example-rendered' class='rust '>
<span class='attribute'>#[<span class='ident'>macro_use</span>(<span class='ident'>parser</span>)]</span>
<span class='kw'>extern</span> <span class='kw'>crate</span> <span class='ident'>libanex</span> ;

<span class='kw'>use</span> <span class='ident'>libanex</span>::<span class='ident'>io</span>::<span class='ident'>Parseable</span> ;
<span class='kw'>use</span> <span class='ident'>libanex</span>::<span class='ident'>io</span>::<span class='ident'>lexer</span>::<span class='ident'>Token</span> ;

<span class='kw'>fn</span> <span class='ident'>main</span>() {

    <span class='macro'>parser</span><span class='macro'>!</span>(
        <span class='kw'>fn</span> <span class='ident'>ocb_blas_ccb</span>: (<span class='ident'>Token</span>, <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>Token</span><span class='op'>&gt;</span>, <span class='ident'>Token</span>) {
            [<span class='string'>&quot;{&quot;</span>], ([ <span class='string'>&quot;bla&quot;</span> ] <span class='op'>+</span>), [<span class='string'>&quot;}&quot;</span>]
        }
    ) ;

    <span class='kw'>match</span> (
        <span class='string'>&quot;{ bla bla }&quot;</span>.<span class='ident'>parsex</span>( <span class='ident'>ocb_blas_ccb</span> ),
        <span class='string'>&quot;{ }&quot;</span>        .<span class='ident'>parsex</span>( <span class='ident'>ocb_blas_ccb</span> ),
        <span class='string'>&quot;{ bli bli }&quot;</span>.<span class='ident'>parsex</span>( <span class='ident'>ocb_blas_ccb</span> ),
    ) {
        ( <span class='prelude-val'>Ok</span>( (_, <span class='ident'>v2</span>, _) ), <span class='prelude-val'>Err</span>(_), <span class='prelude-val'>Err</span>(_) ) <span class='op'>=&gt;</span> {
            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>v2</span>.<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>2</span> ) ;
        },
        _ <span class='op'>=&gt;</span> <span class='macro'>assert</span><span class='macro'>!</span>( <span class='boolval'>false</span> ),
    }

}
</pre>

<p>Possibly empty repetition of <code>&quot;bla&quot;</code>, ignoring the braces.</p>
<pre id='rust-example-rendered' class='rust '>
<span class='attribute'>#[<span class='ident'>macro_use</span>(<span class='ident'>parser</span>)]</span>
<span class='kw'>extern</span> <span class='kw'>crate</span> <span class='ident'>libanex</span> ;

<span class='kw'>use</span> <span class='ident'>libanex</span>::<span class='ident'>io</span>::<span class='ident'>Parseable</span> ;
<span class='kw'>use</span> <span class='ident'>libanex</span>::<span class='ident'>io</span>::<span class='ident'>lexer</span>::<span class='ident'>Token</span> ;

<span class='kw'>fn</span> <span class='ident'>main</span>() {

    <span class='macro'>parser</span><span class='macro'>!</span>(
        <span class='kw'>fn</span> <span class='ident'>ocb_blas_ccb</span>: <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>Token</span><span class='op'>&gt;</span> {
            [<span class='string'>&quot;{&quot;</span>] <span class='op'>&gt;</span> ([ <span class='string'>&quot;bla&quot;</span> ] <span class='op'>*</span>) <span class='op'>&lt;</span> [<span class='string'>&quot;}&quot;</span>]
        }
    ) ;

    <span class='kw'>match</span> (
        <span class='string'>&quot;{ bla bla bla }&quot;</span>.<span class='ident'>parsex</span>( <span class='ident'>ocb_blas_ccb</span> ),
        <span class='string'>&quot;{ }&quot;</span>            .<span class='ident'>parsex</span>( <span class='ident'>ocb_blas_ccb</span> ),
        <span class='string'>&quot;{ {}}}bli bli }&quot;</span>.<span class='ident'>parsex</span>( <span class='ident'>ocb_blas_ccb</span> ),
    ) {
        ( <span class='prelude-val'>Ok</span>(<span class='ident'>v3</span>), <span class='prelude-val'>Ok</span>(<span class='ident'>v0</span>), <span class='prelude-val'>Err</span>(_) ) <span class='op'>=&gt;</span> {
            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>v3</span>.<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>3</span> ) ;
            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>v0</span>.<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>0</span> ) ;
        },
        _ <span class='op'>=&gt;</span> <span class='macro'>assert</span><span class='macro'>!</span>( <span class='boolval'>false</span> ),
    }
}
</pre>

<p>Some more examples.</p>
<pre id='rust-example-rendered' class='rust '>
<span class='attribute'>#[<span class='ident'>macro_use</span>(<span class='ident'>parser</span>)]</span>
<span class='kw'>extern</span> <span class='kw'>crate</span> <span class='ident'>libanex</span> ;

<span class='kw'>use</span> <span class='ident'>libanex</span>::<span class='ident'>io</span>::<span class='ident'>Parseable</span> ;
<span class='kw'>use</span> <span class='ident'>libanex</span>::<span class='ident'>io</span>::<span class='ident'>lexer</span>::<span class='ident'>Token</span> ;

<span class='kw'>fn</span> <span class='ident'>main</span>() {

    <span class='macro'>parser</span><span class='macro'>!</span>( <span class='kw'>fn</span> <span class='ident'>ocb</span> : <span class='ident'>Token</span> { [ <span class='string'>&quot;{&quot;</span> ] } ) ;
    <span class='macro'>parser</span><span class='macro'>!</span>( <span class='kw'>fn</span> <span class='ident'>ccb</span> : <span class='ident'>Token</span> { [ <span class='string'>&quot;}&quot;</span> ] } ) ;

    <span class='macro'>parser</span><span class='macro'>!</span>(
        <span class='kw'>fn</span> <span class='ident'>cb_blas_blis_branch</span>: (<span class='ident'>Token</span>, <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>Token</span><span class='op'>&gt;</span>, <span class='ident'>Token</span>) {
            <span class='op'>|</span> ( <span class='ident'>ocb</span>, ( [<span class='string'>&quot;bli&quot;</span>] <span class='op'>*</span> ), <span class='ident'>ccb</span> )
            <span class='op'>|</span> ( <span class='ident'>ocb</span>, ( [<span class='string'>&quot;bla&quot;</span>] <span class='op'>*</span> ), <span class='ident'>ccb</span> )
        }
    ) ;

    <span class='comment'>// Droping braces using a function, note the destructuring binding.</span>
    <span class='macro'>parser</span><span class='macro'>!</span>(
        <span class='kw'>fn</span> <span class='ident'>blas_blis_branch</span>: <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>Token</span><span class='op'>&gt;</span> {
            <span class='ident'>cb_blas_blis_branch</span> <span class='op'>=&gt;</span> (_, <span class='ident'>vec</span>, _) <span class='op'>=&gt;</span> { <span class='ident'>vec</span> }
        }
    ) ;

    <span class='kw'>match</span> (
        <span class='string'>&quot;{ bla bla bla }&quot;</span>.<span class='ident'>parsex</span>( <span class='ident'>blas_blis_branch</span> ),
        <span class='string'>&quot;{ bli }&quot;</span>        .<span class='ident'>parsex</span>( <span class='ident'>blas_blis_branch</span> ),
        <span class='string'>&quot;{ bli bla bli }&quot;</span>.<span class='ident'>parsex</span>( <span class='ident'>blas_blis_branch</span> ),
    ) {
        ( <span class='prelude-val'>Ok</span>(<span class='ident'>v3</span>), <span class='prelude-val'>Ok</span>(<span class='ident'>v1</span>), <span class='prelude-val'>Err</span>(_) ) <span class='op'>=&gt;</span> {
            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>v3</span>.<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>3</span> ) ;
            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>v1</span>.<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>1</span> ) ;
        },
        _ <span class='op'>=&gt;</span> <span class='macro'>assert</span><span class='macro'>!</span>( <span class='boolval'>false</span> ),
    }


    <span class='comment'>// A bigger, stupidly monolithic, example.</span>

    <span class='kw'>fn</span> <span class='ident'>token_printer</span>( <span class='ident'>tkn</span>: <span class='kw-2'>&amp;</span> <span class='ident'>Token</span> ) {
        <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;  {}&quot;</span>, <span class='ident'>tkn</span>)
    }

    <span class='kw'>fn</span> <span class='ident'>token_vec_printer</span>( <span class='ident'>vec</span>: <span class='kw-2'>&amp;</span> <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>Token</span><span class='op'>&gt;</span> ) {
        <span class='kw'>for</span> <span class='ident'>tkn</span> <span class='kw'>in</span> <span class='ident'>vec</span>.<span class='ident'>iter</span>() {
            <span class='ident'>token_printer</span>(<span class='ident'>tkn</span>)
        }
    }

    <span class='macro'>parser</span><span class='macro'>!</span>(
        <span class='kw'>fn</span> <span class='ident'>stupid_parser</span>: <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>Token</span><span class='op'>&gt;&gt;</span> {
            (
                <span class='comment'>// We are parsing a `*` repetition of the following.</span>
                <span class='comment'>// Parsed but ignored ocb.</span>
                [<span class='string'>&quot;{&quot;</span>] <span class='op'>&gt;</span> (
                    <span class='op'>|</span> (
                        <span class='comment'>// First branch, a `+` rep of `&quot;bla&quot;`.</span>
                        ( [<span class='string'>&quot;bla&quot;</span>] <span class='op'>+</span> ) <span class='op'>=&gt;</span> <span class='ident'>vec</span> <span class='op'>=&gt;</span> {
                            <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;Branch1:&quot;</span>) ;
                            <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;&gt; vec:&quot;</span>) ;
                            <span class='ident'>token_vec_printer</span>(<span class='kw-2'>&amp;</span> <span class='ident'>vec</span>) ;
                            <span class='ident'>vec</span>
                        }
                    )
                    <span class='op'>|</span> (
                        <span class='comment'>// Second branch, a `*` rep of `&quot;bli&quot;` and a `&quot;blu&quot;`.</span>
                        ( ([<span class='string'>&quot;bli&quot;</span>] <span class='op'>*</span>), [<span class='string'>&quot;blu&quot;</span>] )
                        <span class='comment'>// Binding things.</span>
                        <span class='op'>=&gt;</span> (<span class='ident'>vec</span>, <span class='ident'>token</span>) <span class='op'>=&gt;</span> {
                            <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;Branch2:&quot;</span>) ;
                            <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;&gt; vec:&quot;</span>) ;
                            <span class='ident'>token_vec_printer</span>(<span class='kw-2'>&amp;</span> <span class='ident'>vec</span>) ;
                            <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;&gt; token:&quot;</span>) ;
                            <span class='ident'>token_printer</span>(<span class='kw-2'>&amp;</span> <span class='ident'>token</span>) ;
                            <span class='comment'>// Actually dropping the `&quot;blu&quot;`.</span>
                            <span class='ident'>vec</span>
                        }
                    )
                    <span class='op'>|</span> (
                        <span class='comment'>// Third branch, a `&quot;bli&quot;` and a `*` rep of `&quot;bla&quot;`.</span>
                        ([<span class='string'>&quot;bli&quot;</span>], ([<span class='string'>&quot;bla&quot;</span>] <span class='op'>*</span>) )
                        <span class='op'>=&gt;</span> (_, <span class='ident'>vec</span>) <span class='op'>=&gt;</span> {
                            <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;Branch3:&quot;</span>) ;
                            <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;  (discarded first token)&quot;</span>) ;
                            <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;&gt; vec:&quot;</span>) ;
                            <span class='ident'>token_vec_printer</span>(<span class='kw-2'>&amp;</span> <span class='ident'>vec</span>) ;
                            <span class='comment'>// Only returning the repetition.</span>
                            <span class='ident'>vec</span>
                        }
                    )
                <span class='comment'>// Parsed but ignored ccb with some side effects.</span>
                ) <span class='op'>&lt;</span> ([<span class='string'>&quot;}&quot;</span>] <span class='op'>=&gt;</span> <span class='ident'>token</span> <span class='op'>=&gt;</span> {
                    <span class='macro'>println</span><span class='macro'>!</span>(<span class='string'>&quot;Closing curly brace is ignored but parsed:&quot;</span>) ;
                    <span class='ident'>token_printer</span>(<span class='kw-2'>&amp;</span> <span class='ident'>token</span>)
                })

            <span class='comment'>// We are indeed parsing a `*` rep of the body above.</span>
            ) <span class='op'>*</span>
        }
    ) ;

    <span class='kw'>match</span> <span class='string'>&quot; { bli bla bla } { bli bli bli blu } { bla } { blu }&quot;</span>
            .<span class='ident'>parsex</span>( <span class='ident'>stupid_parser</span> ) {

        <span class='prelude-val'>Ok</span>( <span class='ident'>vec_vec</span> ) <span class='op'>=&gt;</span> {
            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>vec_vec</span>.<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>4</span> ) ;

            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>vec_vec</span>[<span class='number'>0</span>].<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>2</span> ) ;
            <span class='kw'>for</span> <span class='ident'>bla</span> <span class='kw'>in</span> <span class='ident'>vec_vec</span>[<span class='number'>0</span>].<span class='ident'>iter</span>() {
                <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>bla</span>.<span class='ident'>tkn</span> <span class='op'>==</span> <span class='string'>&quot;bla&quot;</span> ) ;
            } ;

            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>vec_vec</span>[<span class='number'>1</span>].<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>3</span> ) ;
            <span class='kw'>for</span> <span class='ident'>bli</span> <span class='kw'>in</span> <span class='ident'>vec_vec</span>[<span class='number'>1</span>].<span class='ident'>iter</span>() {
                <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>bli</span>.<span class='ident'>tkn</span> <span class='op'>==</span> <span class='string'>&quot;bli&quot;</span> ) ;
            } ;

            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>vec_vec</span>[<span class='number'>2</span>].<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>1</span> ) ;
            <span class='kw'>for</span> <span class='ident'>bla</span> <span class='kw'>in</span> <span class='ident'>vec_vec</span>[<span class='number'>2</span>].<span class='ident'>iter</span>() {
                <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>bla</span>.<span class='ident'>tkn</span> <span class='op'>==</span> <span class='string'>&quot;bla&quot;</span> ) ;
            } ;

            <span class='macro'>assert</span><span class='macro'>!</span>( <span class='ident'>vec_vec</span>[<span class='number'>3</span>].<span class='ident'>len</span>() <span class='op'>==</span> <span class='number'>0</span> ) ;
        },
        _ <span class='op'>=&gt;</span> <span class='macro'>assert</span><span class='macro'>!</span>( <span class='boolval'>false</span> ),
    }
}
</pre>
</div><h2 id='modules' class='section-header'><a href="#modules">Modules</a></h2>
<table>
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='mod' href='branch/index.html'
                               title='libanex::io::parser::branch'>branch</a></td>
                        <td class='docblock short'><p>Aggregates the parsers obtained by combining several parsers
in branches.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='mod' href='seq/index.html'
                               title='libanex::io::parser::seq'>seq</a></td>
                        <td class='docblock short'><p>Aggregates the parsers obtained by combining several parsers in
sequence.</p>
</td>
                    </tr>
                </table><h2 id='enums' class='section-header'><a href="#enums">Enums</a></h2>
<table>
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='enum' href='enum.ParseFailure.html'
                               title='libanex::io::parser::ParseFailure'>ParseFailure</a></td>
                        <td class='docblock short'><p>Failed parse attempt.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='enum' href='enum.Result.html'
                               title='libanex::io::parser::Result'>Result</a></td>
                        <td class='docblock short'><p>Result of a parse attempt.</p>
</td>
                    </tr>
                </table><h2 id='functions' class='section-header'><a href="#functions">Functions</a></h2>
<table>
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.build_regex.html'
                               title='libanex::io::parser::build_regex'>build_regex</a></td>
                        <td class='docblock short'><p>Builds a regex.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.failure.html'
                               title='libanex::io::parser::failure'>failure</a></td>
                        <td class='docblock short'><p>A parser that always fails.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.fun.html'
                               title='libanex::io::parser::fun'>fun</a></td>
                        <td class='docblock short'><p>Parses <code>parser</code> and applies <code>f</code> to the result.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.ignr_outter.html'
                               title='libanex::io::parser::ignr_outter'>ignr_outter</a></td>
                        <td class='docblock short'><p>Parses <code>left</code>, <code>mid</code> and <code>right</code>. Returns only <code>mid</code>.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.parse.html'
                               title='libanex::io::parser::parse'>parse</a></td>
                        <td class='docblock short'><p>Parses a lexer using <code>parser</code>.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.parse_all.html'
                               title='libanex::io::parser::parse_all'>parse_all</a></td>
                        <td class='docblock short'><p>Parses a lexer using <code>parser</code>. Fails if the lexer is not
completely consumed by <code>parser</code>.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.regex.html'
                               title='libanex::io::parser::regex'>regex</a></td>
                        <td class='docblock short'><p>Regex parser.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.rep.html'
                               title='libanex::io::parser::rep'>rep</a></td>
                        <td class='docblock short'><p>Parses a potentially empty repetition of a parser.
Always succeeds.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.rep1.html'
                               title='libanex::io::parser::rep1'>rep1</a></td>
                        <td class='docblock short'><p>Parses a non-empty repetition of a parser.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.success.html'
                               title='libanex::io::parser::success'>success</a></td>
                        <td class='docblock short'><p>A parser that always succeeds.</p>
</td>
                    </tr>
                
                    <tr>
                        <td><a class='stability Unmarked' title='No stability level'></a><a class='fn' href='fn.token.html'
                               title='libanex::io::parser::token'>token</a></td>
                        <td class='docblock short'><p>Token parser.</p>
</td>
                    </tr>
                </table></section>
    <section id='search' class="content hidden"></section>

    <section class="footer"></section>

    <div id="help" class="hidden">
        <div class="shortcuts">
            <h1>Keyboard shortcuts</h1>
            <dl>
                <dt>?</dt>
                <dd>Show this help dialog</dd>
                <dt>S</dt>
                <dd>Focus the search field</dd>
                <dt>&larrb;</dt>
                <dd>Move up in search results</dd>
                <dt>&rarrb;</dt>
                <dd>Move down in search results</dd>
                <dt>&#9166;</dt>
                <dd>Go to active search result</dd>
            </dl>
        </div>
        <div class="infos">
            <h1>Search tricks</h1>
            <p>
                Prefix searches with a type followed by a colon (e.g.
                <code>fn:</code>) to restrict the search to a given type.
            </p>
            <p>
                Accepted types are: <code>fn</code>, <code>mod</code>,
                <code>struct</code>, <code>enum</code>,
                <code>trait</code>, <code>typedef</code> (or
                <code>tdef</code>).
            </p>
        </div>
    </div>

    

    <script>
        window.rootPath = "../../../";
        window.currentCrate = "libanex";
        window.playgroundUrl = "";
    </script>
    <script src="../../../jquery.js"></script>
    <script src="../../../main.js"></script>
    
    <script async src="../../../search-index.js"></script>
</body>
</html>